﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WoWServer
{
    public partial class AboutBox : Form
    {
        public AboutBox()
        {
            InitializeComponent();

            label2.Text = "Version: " + Application.ProductVersion.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/MidgetMan/trinitycore_admin");
        }
    }
}
