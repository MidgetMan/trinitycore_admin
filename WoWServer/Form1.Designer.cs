﻿namespace WoWServer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.MainStrip = new System.Windows.Forms.ToolStrip();
            this.FileBTN = new System.Windows.Forms.ToolStripDropDownButton();
            this.CheckForUpdateBTN = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutBTN = new System.Windows.Forms.ToolStripButton();
            this.UpdateButton = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LogBox = new System.Windows.Forms.RichTextBox();
            this.StopApacheBTN = new System.Windows.Forms.Button();
            this.StartApacheBTN = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.StopMYSQLBTN = new System.Windows.Forms.Button();
            this.StartMYSQLBTN = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.StopWorld = new System.Windows.Forms.Button();
            this.StartWorld = new System.Windows.Forms.Button();
            this.StopAuth = new System.Windows.Forms.Button();
            this.StartAuth = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.RestoreAuth = new System.Windows.Forms.Button();
            this.BackupBoxAuth = new System.Windows.Forms.ComboBox();
            this.ASConfBTN = new System.Windows.Forms.Button();
            this.RestoreWorld = new System.Windows.Forms.Button();
            this.BackupBoxWorld = new System.Windows.Forms.ComboBox();
            this.WSConfBTN = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.MinimizeTrayCheck = new System.Windows.Forms.CheckBox();
            this.ApachePathBox = new System.Windows.Forms.TextBox();
            this.ApachePathBTN = new System.Windows.Forms.Button();
            this.MySQLDPathBox = new System.Windows.Forms.TextBox();
            this.MySQLDPathBTN = new System.Windows.Forms.Button();
            this.ServerPathBTN = new System.Windows.Forms.Button();
            this.PathBox = new System.Windows.Forms.TextBox();
            this.CheckServ = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.MainStrip.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainStrip
            // 
            this.MainStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileBTN,
            this.AboutBTN,
            this.UpdateButton});
            this.MainStrip.Location = new System.Drawing.Point(0, 0);
            this.MainStrip.Name = "MainStrip";
            this.MainStrip.Size = new System.Drawing.Size(341, 25);
            this.MainStrip.TabIndex = 0;
            this.MainStrip.Text = "toolStrip1";
            // 
            // FileBTN
            // 
            this.FileBTN.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.FileBTN.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CheckForUpdateBTN,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.FileBTN.Image = ((System.Drawing.Image)(resources.GetObject("FileBTN.Image")));
            this.FileBTN.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FileBTN.Name = "FileBTN";
            this.FileBTN.Size = new System.Drawing.Size(38, 22);
            this.FileBTN.Text = "File";
            // 
            // CheckForUpdateBTN
            // 
            this.CheckForUpdateBTN.Name = "CheckForUpdateBTN";
            this.CheckForUpdateBTN.Size = new System.Drawing.Size(166, 22);
            this.CheckForUpdateBTN.Text = "Check for Update";
            this.CheckForUpdateBTN.Click += new System.EventHandler(this.CheckForUpdateBTN_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(163, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // AboutBTN
            // 
            this.AboutBTN.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.AboutBTN.Image = ((System.Drawing.Image)(resources.GetObject("AboutBTN.Image")));
            this.AboutBTN.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AboutBTN.Name = "AboutBTN";
            this.AboutBTN.Size = new System.Drawing.Size(44, 22);
            this.AboutBTN.Text = "About";
            this.AboutBTN.Click += new System.EventHandler(this.AboutBTN_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.UpdateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UpdateButton.Image = global::WoWServer.Properties.Resources._1430425143_Information;
            this.UpdateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.UpdateButton.Size = new System.Drawing.Size(23, 22);
            this.UpdateButton.Text = "toolStripButton1";
            this.UpdateButton.ToolTipText = "Update TC Control Panel";
            this.UpdateButton.Visible = false;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(341, 431);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LogBox);
            this.tabPage1.Controls.Add(this.StopApacheBTN);
            this.tabPage1.Controls.Add(this.StartApacheBTN);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.StopMYSQLBTN);
            this.tabPage1.Controls.Add(this.StartMYSQLBTN);
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.StopWorld);
            this.tabPage1.Controls.Add(this.StartWorld);
            this.tabPage1.Controls.Add(this.StopAuth);
            this.tabPage1.Controls.Add(this.StartAuth);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(333, 405);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Server";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LogBox
            // 
            this.LogBox.Location = new System.Drawing.Point(6, 250);
            this.LogBox.Name = "LogBox";
            this.LogBox.ReadOnly = true;
            this.LogBox.Size = new System.Drawing.Size(321, 149);
            this.LogBox.TabIndex = 19;
            this.LogBox.Text = "";
            // 
            // StopApacheBTN
            // 
            this.StopApacheBTN.Location = new System.Drawing.Point(232, 198);
            this.StopApacheBTN.Name = "StopApacheBTN";
            this.StopApacheBTN.Size = new System.Drawing.Size(93, 46);
            this.StopApacheBTN.TabIndex = 18;
            this.StopApacheBTN.Text = "Stop";
            this.StopApacheBTN.UseVisualStyleBackColor = true;
            this.StopApacheBTN.Click += new System.EventHandler(this.StopApacheBTN_Click);
            // 
            // StartApacheBTN
            // 
            this.StartApacheBTN.Enabled = false;
            this.StartApacheBTN.Location = new System.Drawing.Point(53, 198);
            this.StartApacheBTN.Name = "StartApacheBTN";
            this.StartApacheBTN.Size = new System.Drawing.Size(173, 46);
            this.StartApacheBTN.TabIndex = 17;
            this.StartApacheBTN.Text = "Start Apache";
            this.StartApacheBTN.UseVisualStyleBackColor = true;
            this.StartApacheBTN.Click += new System.EventHandler(this.StartApacheBTN_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::WoWServer.Properties.Resources.pb_feather;
            this.pictureBox4.Location = new System.Drawing.Point(6, 198);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(41, 46);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 16;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::WoWServer.Properties.Resources.dbs_mysql;
            this.pictureBox3.Location = new System.Drawing.Point(6, 146);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(41, 46);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // StopMYSQLBTN
            // 
            this.StopMYSQLBTN.Location = new System.Drawing.Point(232, 146);
            this.StopMYSQLBTN.Name = "StopMYSQLBTN";
            this.StopMYSQLBTN.Size = new System.Drawing.Size(93, 46);
            this.StopMYSQLBTN.TabIndex = 14;
            this.StopMYSQLBTN.Text = "Stop";
            this.StopMYSQLBTN.UseVisualStyleBackColor = true;
            this.StopMYSQLBTN.Click += new System.EventHandler(this.StopMYSQLBTN_Click);
            // 
            // StartMYSQLBTN
            // 
            this.StartMYSQLBTN.Enabled = false;
            this.StartMYSQLBTN.Location = new System.Drawing.Point(53, 146);
            this.StartMYSQLBTN.Name = "StartMYSQLBTN";
            this.StartMYSQLBTN.Size = new System.Drawing.Size(173, 46);
            this.StartMYSQLBTN.TabIndex = 13;
            this.StartMYSQLBTN.Text = "Start MySQL";
            this.StartMYSQLBTN.UseVisualStyleBackColor = true;
            this.StartMYSQLBTN.Click += new System.EventHandler(this.StartMYSQLBTN_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(200, 110);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(125, 17);
            this.checkBox2.TabIndex = 10;
            this.checkBox2.Text = "Hide Output Window";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 110);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(144, 17);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Restart Servers on Crash";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // StopWorld
            // 
            this.StopWorld.Location = new System.Drawing.Point(232, 58);
            this.StopWorld.Name = "StopWorld";
            this.StopWorld.Size = new System.Drawing.Size(93, 46);
            this.StopWorld.TabIndex = 9;
            this.StopWorld.Text = "Stop";
            this.StopWorld.UseVisualStyleBackColor = true;
            this.StopWorld.Click += new System.EventHandler(this.StopWorld_Click);
            // 
            // StartWorld
            // 
            this.StartWorld.Enabled = false;
            this.StartWorld.Location = new System.Drawing.Point(53, 58);
            this.StartWorld.Name = "StartWorld";
            this.StartWorld.Size = new System.Drawing.Size(173, 46);
            this.StartWorld.TabIndex = 8;
            this.StartWorld.Text = "Start WorldServer";
            this.StartWorld.UseVisualStyleBackColor = true;
            this.StartWorld.Click += new System.EventHandler(this.StartWorld_Click);
            // 
            // StopAuth
            // 
            this.StopAuth.Location = new System.Drawing.Point(232, 6);
            this.StopAuth.Name = "StopAuth";
            this.StopAuth.Size = new System.Drawing.Size(93, 46);
            this.StopAuth.TabIndex = 7;
            this.StopAuth.Text = "Stop";
            this.StopAuth.UseVisualStyleBackColor = true;
            this.StopAuth.Click += new System.EventHandler(this.StopAuth_Click);
            // 
            // StartAuth
            // 
            this.StartAuth.Enabled = false;
            this.StartAuth.Location = new System.Drawing.Point(53, 6);
            this.StartAuth.Name = "StartAuth";
            this.StartAuth.Size = new System.Drawing.Size(173, 46);
            this.StartAuth.TabIndex = 6;
            this.StartAuth.Text = "Start AuthServer";
            this.StartAuth.UseVisualStyleBackColor = true;
            this.StartAuth.Click += new System.EventHandler(this.StartAuth_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::WoWServer.Properties.Resources.worldserver;
            this.pictureBox2.Location = new System.Drawing.Point(6, 58);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(41, 46);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WoWServer.Properties.Resources.bnetserver;
            this.pictureBox1.Location = new System.Drawing.Point(6, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(41, 46);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.RestoreAuth);
            this.tabPage2.Controls.Add(this.BackupBoxAuth);
            this.tabPage2.Controls.Add(this.ASConfBTN);
            this.tabPage2.Controls.Add(this.RestoreWorld);
            this.tabPage2.Controls.Add(this.BackupBoxWorld);
            this.tabPage2.Controls.Add(this.WSConfBTN);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(333, 405);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configs";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // RestoreAuth
            // 
            this.RestoreAuth.Location = new System.Drawing.Point(222, 123);
            this.RestoreAuth.Name = "RestoreAuth";
            this.RestoreAuth.Size = new System.Drawing.Size(103, 28);
            this.RestoreAuth.TabIndex = 5;
            this.RestoreAuth.Text = "Restore Backup";
            this.RestoreAuth.UseVisualStyleBackColor = true;
            this.RestoreAuth.Click += new System.EventHandler(this.RestoreAuth_Click);
            // 
            // BackupBoxAuth
            // 
            this.BackupBoxAuth.FormattingEnabled = true;
            this.BackupBoxAuth.Location = new System.Drawing.Point(154, 96);
            this.BackupBoxAuth.Name = "BackupBoxAuth";
            this.BackupBoxAuth.Size = new System.Drawing.Size(171, 21);
            this.BackupBoxAuth.TabIndex = 4;
            this.BackupBoxAuth.Text = "Select Backup...";
            // 
            // ASConfBTN
            // 
            this.ASConfBTN.Location = new System.Drawing.Point(6, 96);
            this.ASConfBTN.Name = "ASConfBTN";
            this.ASConfBTN.Size = new System.Drawing.Size(142, 55);
            this.ASConfBTN.TabIndex = 3;
            this.ASConfBTN.Text = "Authserver Config";
            this.ASConfBTN.UseVisualStyleBackColor = true;
            this.ASConfBTN.Click += new System.EventHandler(this.ASConfBTN_Click);
            // 
            // RestoreWorld
            // 
            this.RestoreWorld.Location = new System.Drawing.Point(222, 33);
            this.RestoreWorld.Name = "RestoreWorld";
            this.RestoreWorld.Size = new System.Drawing.Size(103, 28);
            this.RestoreWorld.TabIndex = 2;
            this.RestoreWorld.Text = "Restore Backup";
            this.RestoreWorld.UseVisualStyleBackColor = true;
            this.RestoreWorld.Click += new System.EventHandler(this.RestoreWorld_Click);
            // 
            // BackupBoxWorld
            // 
            this.BackupBoxWorld.FormattingEnabled = true;
            this.BackupBoxWorld.Location = new System.Drawing.Point(154, 6);
            this.BackupBoxWorld.Name = "BackupBoxWorld";
            this.BackupBoxWorld.Size = new System.Drawing.Size(171, 21);
            this.BackupBoxWorld.TabIndex = 1;
            this.BackupBoxWorld.Text = "Select Backup...";
            // 
            // WSConfBTN
            // 
            this.WSConfBTN.Location = new System.Drawing.Point(6, 6);
            this.WSConfBTN.Name = "WSConfBTN";
            this.WSConfBTN.Size = new System.Drawing.Size(142, 55);
            this.WSConfBTN.TabIndex = 0;
            this.WSConfBTN.Text = "WorldServer Config";
            this.WSConfBTN.UseVisualStyleBackColor = true;
            this.WSConfBTN.Click += new System.EventHandler(this.WSConfBTN_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.MinimizeTrayCheck);
            this.tabPage3.Controls.Add(this.ApachePathBox);
            this.tabPage3.Controls.Add(this.ApachePathBTN);
            this.tabPage3.Controls.Add(this.MySQLDPathBox);
            this.tabPage3.Controls.Add(this.MySQLDPathBTN);
            this.tabPage3.Controls.Add(this.ServerPathBTN);
            this.tabPage3.Controls.Add(this.PathBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(333, 405);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Options";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // MinimizeTrayCheck
            // 
            this.MinimizeTrayCheck.AutoSize = true;
            this.MinimizeTrayCheck.Location = new System.Drawing.Point(6, 112);
            this.MinimizeTrayCheck.Name = "MinimizeTrayCheck";
            this.MinimizeTrayCheck.Size = new System.Drawing.Size(102, 17);
            this.MinimizeTrayCheck.TabIndex = 19;
            this.MinimizeTrayCheck.Text = "Minimize to Tray";
            this.MinimizeTrayCheck.UseVisualStyleBackColor = true;
            this.MinimizeTrayCheck.CheckedChanged += new System.EventHandler(this.MinimizeTrayCheck_CheckedChanged);
            // 
            // ApachePathBox
            // 
            this.ApachePathBox.Location = new System.Drawing.Point(101, 66);
            this.ApachePathBox.Name = "ApachePathBox";
            this.ApachePathBox.Size = new System.Drawing.Size(224, 20);
            this.ApachePathBox.TabIndex = 18;
            this.ApachePathBox.TextChanged += new System.EventHandler(this.ApachePathBox_TextChanged);
            // 
            // ApachePathBTN
            // 
            this.ApachePathBTN.Location = new System.Drawing.Point(6, 64);
            this.ApachePathBTN.Name = "ApachePathBTN";
            this.ApachePathBTN.Size = new System.Drawing.Size(89, 23);
            this.ApachePathBTN.TabIndex = 17;
            this.ApachePathBTN.Text = "Apache Path";
            this.ApachePathBTN.UseVisualStyleBackColor = true;
            this.ApachePathBTN.Click += new System.EventHandler(this.ApachePathBTN_Click);
            // 
            // MySQLDPathBox
            // 
            this.MySQLDPathBox.Location = new System.Drawing.Point(101, 37);
            this.MySQLDPathBox.Name = "MySQLDPathBox";
            this.MySQLDPathBox.Size = new System.Drawing.Size(224, 20);
            this.MySQLDPathBox.TabIndex = 16;
            this.MySQLDPathBox.TextChanged += new System.EventHandler(this.MySQLDPath_TextChanged);
            // 
            // MySQLDPathBTN
            // 
            this.MySQLDPathBTN.Location = new System.Drawing.Point(6, 35);
            this.MySQLDPathBTN.Name = "MySQLDPathBTN";
            this.MySQLDPathBTN.Size = new System.Drawing.Size(89, 23);
            this.MySQLDPathBTN.TabIndex = 15;
            this.MySQLDPathBTN.Text = "MySQL Path";
            this.MySQLDPathBTN.UseVisualStyleBackColor = true;
            this.MySQLDPathBTN.Click += new System.EventHandler(this.MySQLDPathBTN_Click);
            // 
            // ServerPathBTN
            // 
            this.ServerPathBTN.Location = new System.Drawing.Point(6, 6);
            this.ServerPathBTN.Name = "ServerPathBTN";
            this.ServerPathBTN.Size = new System.Drawing.Size(89, 23);
            this.ServerPathBTN.TabIndex = 14;
            this.ServerPathBTN.Text = "Server Path";
            this.ServerPathBTN.UseVisualStyleBackColor = true;
            this.ServerPathBTN.Click += new System.EventHandler(this.button1_Click);
            // 
            // PathBox
            // 
            this.PathBox.Location = new System.Drawing.Point(101, 8);
            this.PathBox.Name = "PathBox";
            this.PathBox.Size = new System.Drawing.Size(224, 20);
            this.PathBox.TabIndex = 13;
            this.PathBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // CheckServ
            // 
            this.CheckServ.Interval = 5000;
            this.CheckServ.Tick += new System.EventHandler(this.CheckServ_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "Everything is still Running. Just in the tray now!";
            this.notifyIcon1.BalloonTipTitle = "WoW Server in Tray!";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "WoWServer";
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(142, 55);
            this.button1.TabIndex = 6;
            this.button1.Text = "WorldConf Generator";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 456);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.MainStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "TC Control Panel V:";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.MainStrip.ResumeLayout(false);
            this.MainStrip.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip MainStrip;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button StopWorld;
        private System.Windows.Forms.Button StartWorld;
        private System.Windows.Forms.Button StopAuth;
        private System.Windows.Forms.Button StartAuth;
        private System.Windows.Forms.ToolStripDropDownButton FileBTN;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Timer CheckServ;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button WSConfBTN;
        private System.Windows.Forms.ComboBox BackupBoxWorld;
        private System.Windows.Forms.Button RestoreWorld;
        private System.Windows.Forms.Button RestoreAuth;
        private System.Windows.Forms.ComboBox BackupBoxAuth;
        private System.Windows.Forms.Button ASConfBTN;
        private System.Windows.Forms.ToolStripButton AboutBTN;
        private System.Windows.Forms.ToolStripButton UpdateButton;
        private System.Windows.Forms.Button StopMYSQLBTN;
        private System.Windows.Forms.Button StartMYSQLBTN;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox MySQLDPathBox;
        private System.Windows.Forms.Button MySQLDPathBTN;
        private System.Windows.Forms.Button ServerPathBTN;
        private System.Windows.Forms.TextBox PathBox;
        private System.Windows.Forms.ToolStripMenuItem CheckForUpdateBTN;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button StopApacheBTN;
        private System.Windows.Forms.Button StartApacheBTN;
        private System.Windows.Forms.TextBox ApachePathBox;
        private System.Windows.Forms.Button ApachePathBTN;
        private System.Windows.Forms.RichTextBox LogBox;
        private System.Windows.Forms.CheckBox MinimizeTrayCheck;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button button1;

    }
}

