﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace WoWServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Text = this.Text + Application.ProductVersion;
            PathBox.Text = WoWServer.Default.PathServer;
            MySQLDPathBox.Text = WoWServer.Default.MySQLDPath;
            ApachePathBox.Text = WoWServer.Default.APACHEPath;
            MinimizeTrayCheck.Checked = WoWServer.Default.MinimizeTray;

            if (WoWServer.Default.FirstRun == false)
            {
                getWorldServBak();
                getAuthServBak();

                if (File.Exists("Update.exe"))
                    File.Delete("Update.exe");
            }

            if (WoWServer.Default.FirstRun == true)
            {
                tabControl1.SelectedTab = tabPage3;
            }

            checkForUpdate();
        }

        private void checkForUpdate()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("http://hempton.us/TC_Admin/update.xml");

                XmlNode node = doc.SelectSingleNode("/item/version");

                if (Application.ProductVersion != node.InnerText)
                {
                    UpdateButton.Visible = true;
                }
            }
            catch
            {

            }
        }

        public void getWorldServBak()
        {
            //string[] files = System.IO.Directory.GetFiles(PathBox.Text, "worldserver.conf.bak*");

            //this.comboBox1.Items.AddRange(files);
            try
            {
                BackupBoxWorld.Items.Clear();

                BackupBoxWorld.Text = "Select Backup...";

                foreach (String file in System.IO.Directory.GetFiles(PathBox.Text, "worldserver.conf.bak*"))
                {
                    BackupBoxWorld.Items.Add(new System.IO.FileInfo(file).Name);
                }
            }
            catch
            {
                //
            }
        }

        public void getAuthServBak()
        {
            try
            {
                //string[] files = System.IO.Directory.GetFiles(PathBox.Text, "worldserver.conf.bak*");

                //this.comboBox1.Items.AddRange(files);

                BackupBoxAuth.Items.Clear();

                BackupBoxAuth.Text = "Select Backup...";

                foreach (String file in System.IO.Directory.GetFiles(PathBox.Text, "authserver.conf.bak*"))
                {
                    BackupBoxAuth.Items.Add(new System.IO.FileInfo(file).Name);
                }
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog me = new FolderBrowserDialog())
            {
                if(me.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    PathBox.Text = me.SelectedPath;
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (File.Exists(PathBox.Text + "\\authserver.exe") && File.Exists(PathBox.Text + "\\worldserver.exe"))
            {
                PathBox.BackColor = Color.LightGreen;
                WoWServer.Default.PathServer = PathBox.Text;
                WoWServer.Default.FirstRun = false;
                
                WoWServer.Default.Save();
                
                StartAuth.Enabled = true;
                StartWorld.Enabled = true;

                getWorldServBak();
                getAuthServBak();
                
            }
            else
            {
                PathBox.BackColor = Color.Pink;
                StartWorld.Enabled = false;
                StartAuth.Enabled = false;
                WoWServer.Default.PathServer = "";
                WoWServer.Default.FirstRun = true;

                WoWServer.Default.Save();
            }
        }

        private void StartAuth_Click(object sender, EventArgs e)
        {
            StartServ(@"\authserver.exe", PathBox.Text, "AuthServer");
        }

        public void StartServ(string EXE, string Path, string title)
        {
            Process myProcess = new Process();
            myProcess.StartInfo.FileName = Path + EXE;
            myProcess.StartInfo.WorkingDirectory = Path;
            
            if(EXE == "\\mysqld.exe")
            {
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.CreateNoWindow = true;

                myProcess.StartInfo.Arguments = "--defaults-file=my.ini";
            }

            if (EXE == "\\httpd.exe")
            {
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.CreateNoWindow = true;
            }

            if (checkBox2.Checked == true)
            {
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.CreateNoWindow = true;
            }

            myProcess.Start();

            LogBox.AppendText("["+DateTime.Now.ToLongTimeString() + "] " + title + " Started! \r\n");
        }


        private void StopAuth_Click(object sender, EventArgs e)
        {
            StopServ("authserver", "AuthServer");
        }

        public void StopServ(string EXE, string title)
        {
          try
            {
                foreach (Process proc in Process.GetProcessesByName(EXE))
                {
                    proc.Kill();
                    LogBox.AppendText("["+DateTime.Now.ToLongTimeString() + "] " + title + " Stopped! \r\n");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void CheckServers(string EXE, string PATH, string EXEProcc, string T)
        {
            var runningProcessByName = Process.GetProcessesByName(EXEProcc);
            if (runningProcessByName.Length == 0)
            {
                StartServ(EXE, PATH, T);
            }
        }

        private void StartWorld_Click(object sender, EventArgs e)
        {
            StartServ(@"\worldserver.exe", PathBox.Text, "WorldServer");
        }

        private void StopWorld_Click(object sender, EventArgs e)
        {
            StopServ("worldserver", "WorldServer");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                CheckServ.Start();
            }
            else
            {
                CheckServ.Stop();
            }
        }

        private void CheckServ_Tick(object sender, EventArgs e)
        {
            CheckServers(@"\worldserver.exe", PathBox.Text, "worldserver", "WorldServer");
            CheckServers(@"\authserver.exe", PathBox.Text, "authserver", "AuthServer");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void WSConfBTN_Click(object sender, EventArgs e)
        {
            LoadConf ConfLoad = new LoadConf();
            ConfLoad.frm1 = this;
            ConfLoad.Show(PathBox.Text+"\\worldserver.conf", "Worldserver.conf");
        }

        private void ASConfBTN_Click(object sender, EventArgs e)
        {
            LoadConf ConfLoad = new LoadConf();
            ConfLoad.frm1 = this;
            ConfLoad.Show(PathBox.Text + "\\authserver.conf", "authserver.conf");
        }

        private void RestoreWorld_Click(object sender, EventArgs e)
        {
            File.Delete(PathBox.Text + "\\worldserver.conf.old");
            File.Copy(PathBox.Text + "\\worldserver.conf", PathBox.Text + "\\worldserver.conf.old");
            File.Delete(PathBox.Text + "\\worldserver.conf");
            File.Copy(PathBox.Text + "\\" + BackupBoxWorld.Text, PathBox.Text + "\\worldserver.conf");

            MessageBox.Show("Restoration Complete, Backupfile: " + BackupBoxWorld.Text + " Is now the Default Config for Worldserver", "Restoration Complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            getWorldServBak();
        }

        private void RestoreAuth_Click(object sender, EventArgs e)
        {
            {
                File.Delete(PathBox.Text + "\\authserver.conf.old");
                File.Copy(PathBox.Text + "\\authserver.conf", PathBox.Text + "\\authserver.conf.old");
                File.Delete(PathBox.Text + "\\authserver.conf");
                File.Copy(PathBox.Text + "\\" + BackupBoxAuth.Text, PathBox.Text + "\\authserver.conf");

                MessageBox.Show("Restoration Complete, Backupfile: " + BackupBoxAuth.Text + " Is now the Default Config for Authserver", "Restoration Complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                getAuthServBak();
            }
        }

        private void AboutBTN_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            AutoUpdaterDotNET.AutoUpdater.Start("http://hempton.us/TC_Admin/update.xml");
        }

        private void MySQLDPath_TextChanged(object sender, EventArgs e)
        {
            if(File.Exists(MySQLDPathBox.Text + "\\mysqld.exe"))
            {
                MySQLDPathBox.BackColor = Color.LightGreen;
                WoWServer.Default.MySQLDPath = MySQLDPathBox.Text;

                StartMYSQLBTN.Enabled = true;
                
                WoWServer.Default.Save();
                
            }
            else
            {
                MySQLDPathBox.BackColor = Color.Pink;
                WoWServer.Default.MySQLDPath = "";


                StartMYSQLBTN.Enabled = false;

                WoWServer.Default.Save();
            }
        }

        private void StartMYSQLBTN_Click(object sender, EventArgs e)
        {
            StartServ("\\mysqld.exe", MySQLDPathBox.Text, "MySQL");
        }

        private void CheckForUpdateBTN_Click(object sender, EventArgs e)
        {
            checkForUpdate();
        }

        private void StartApacheBTN_Click(object sender, EventArgs e)
        {
            StartServ("\\httpd.exe", ApachePathBox.Text, "Apache");
        }

        private void ApachePathBox_TextChanged(object sender, EventArgs e)
        {

            if (File.Exists(ApachePathBox.Text + "\\httpd.exe"))
            {
                ApachePathBox.BackColor = Color.LightGreen;
                WoWServer.Default.APACHEPath = ApachePathBox.Text;

                StartApacheBTN.Enabled = true;

                WoWServer.Default.Save();

            }
            else
            {
                ApachePathBox.BackColor = Color.Pink;
                WoWServer.Default.PathServer = "";


                StartApacheBTN.Enabled = false;

                WoWServer.Default.Save();
            }
        }

        private void StopMYSQLBTN_Click(object sender, EventArgs e)
        {
            StopServ("mysqld", "MySQL");
        }

        private void StopApacheBTN_Click(object sender, EventArgs e)
        {
            StopServ("httpd", "Apache");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            WoWServer.Default.Save();
        }

        private void MySQLDPathBTN_Click(object sender, EventArgs e)
        {

            using (FolderBrowserDialog me = new FolderBrowserDialog())
            {
                if (me.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    MySQLDPathBox.Text = me.SelectedPath;
                }
            }
        }

        private void ApachePathBTN_Click(object sender, EventArgs e)
        {

            using (FolderBrowserDialog me = new FolderBrowserDialog())
            {
                if (me.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ApachePathBox.Text = me.SelectedPath;
                }
            }
        }

        private void MinimizeTrayCheck_CheckedChanged(object sender, EventArgs e)
        {
            WoWServer.Default.MinimizeTray = MinimizeTrayCheck.Checked;
            WoWServer.Default.Save();
        }


        private void Form1_Resize(object sender, EventArgs e)
        {
            if (MinimizeTrayCheck.Checked == true)
            {
                if (FormWindowState.Minimized == this.WindowState)
                {
                    this.ShowInTaskbar = false;
                    notifyIcon1.Visible = true;
                    notifyIcon1.ShowBalloonTip(500);
                    this.Hide();
                }

                else if (FormWindowState.Normal == this.WindowState)
                {
                    notifyIcon1.Visible = false;
                }
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Visible = true;
            this.ShowInTaskbar = true;
        }
    }
}