﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace WoWServer
{
    public partial class LoadConf : Form
    {
        public string docPath;
        public bool CheckBak;
        public string DocTitle;

        public Form1 frm1;

        public LoadConf()
        {
            InitializeComponent();

            CheckBak = false;
        }

        internal void Show(string docpath, string title)
        {
            using (System.IO.StreamReader sr = new System.IO.StreamReader(docpath))
            {
                richTextBox1.Text = sr.ReadToEnd();
            }
            //Assign received parameter(s) to local context

            docPath = docpath;

            DocTitle = title;

            this.Text = "Loaded: " + title;

            this.ShowDialog(); //Display and activate this form (Form2)
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            StreamWriter sw = File.CreateText(docPath);
            foreach (String s in richTextBox1.Lines)
            {
                sw.WriteLine(s);
            }
            sw.Flush();
            sw.Close();
        }

        private void richTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (CheckBak == false)
            {
                //Backup Config File Dialog.
                DialogResult dialogResult = MessageBox.Show("It's advised to backup the config file before you edit it.", "Back Current Config?", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    StreamWriter sw = File.CreateText(docPath + ".bak - " + DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss"));
                    foreach (String s in richTextBox1.Lines)
                    {
                        sw.WriteLine(s);
                    }
                    sw.Flush();
                    sw.Close();

                    CheckBak = true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    //do something else
                    CheckBak = true;
                }
            }
        }

        private void LoadConf_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (DocTitle == "Worldserver.conf")
            {
                ((Form1)frm1).getWorldServBak();
            }
            else
            {
                ((Form1)frm1).getAuthServBak();
            }
        }
    }
}
