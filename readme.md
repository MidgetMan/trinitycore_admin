# TC Control Panel

TC Control Panel is being designed in mind to be used on Windows Machines, maybe be possible to use via Mono, but I have no Idea. 
Current Features:

  - Start/Stop WorldServer and AuthServer.
  - Servers Restart on Crash
  - Edit Config files for both Auth & World Server
  - Backup and restore Config files
  - Hide Window output from User (*Note: For now you will not be able to do commands to WorldServer window)

###Version: 0.0.1.0


##Requirements:
 - .NET Version 4.0
 - Visual Studio 2013 for Compiling
 
###Contributers
-MidgetMan