﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Web;

namespace WorldConf_Gen
{
    public partial class Form1 : Form
    {
        private string conn;
        private MySqlConnection connect;

        public Form1()
        {
            InitializeComponent();
            button2.Enabled = false;
        }

        public void DB_Connection(string url, string username, string password,int port)
        {
            try
            {
                conn = "Server="+url+";Port="+port.ToString()+";Uid=" + username +";Pwd="+password+";";
                connect = new MySqlConnection(conn);
                connect.Open();
            }
            catch (MySqlException e)
            {
                throw;
            }
        }

        public bool validate_DB(string Database)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = @DB";
            cmd.Parameters.AddWithValue("@DB", Database);
            cmd.Connection = connect;
            MySqlDataReader DB_Check = cmd.ExecuteReader();
            if (DB_Check.Read())
            {
                connect.Close();
                return true;
            }
            else
            {
                connect.Close();
                return false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = (tabControl1.SelectedIndex + 1 < tabControl1.TabCount) ? tabControl1.SelectedIndex + 1 : 0;
            button2.Enabled = true;

            if (tabControl1.SelectedIndex == (tabControl1.TabCount - 1))
                button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = (tabControl1.SelectedIndex - 1 < tabControl1.TabCount) ? tabControl1.SelectedIndex - 1 : 0;
            button1.Enabled = true;

            if (tabControl1.SelectedIndex == 0)
                button2.Enabled = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if(validate_DB(textBox3.Text) == true)
            {
                textBox3.BackColor = Color.LightGreen;
            }
            else
            {
                textBox3.BackColor = Color.LightPink;
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            DB_Connection(textBox1.Text, textBox2.Text, maskedTextBox1.Text, (int)numericUpDown1.Value);

            checkConnection();
        }
    }
}
